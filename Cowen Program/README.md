# New Employee Sheet Wizard v1.0

The NESW, or "Neh-soo" as the kids pronounce it, is a Java-based application meant to automate the Welcome sheet creation process. It comes with two modes:

  - Single Sheet Creation mode
  - Multi-sheet Creation mode

And can utilize both .txt and .csv documents in order to create User Welcome Sheets!

### Running

Running NESW is fairly straightforward; simply run the JAR file as you would run any other file. If your computer doesn't recognize the JAR file or it doesn't seem to run, open up the command line, navigate to the directory of the application (using the cd command), and type 

```sh
$ java -jar "New Employee Sheet Wizard.jar"
```

A window should pop up with three options: Single User, Multiple Users, and Exit. If this does not occur or if the program still won't open, feel free to contact the creator for troubleshooting and possible bug fixes.
### How to Use

This is perhaps the most important part of the README file you are currently perusing. This section is so unbelivably essential due to NESW's particular template format. For one, NESW bases all of the Word documents it creates on a single Word doc, **New_Employee_Sheet_Template.docx**. If a file with this name and extension is not present in the same directory as NESW, it will not create any sheets.

Secondly, the template doc must contain the following variables:

* **$name** - First name
* **$username** - Username
* **$email** - Email
* **$number** - Phone number
* **$isa** - Associated ISA number

If any of these variables are not present, the program will still work, however all the info may not be present in the created document. 

Last, but not least, each employee sheet requires certain information. First name, last name, and ISA number are all required; the other three tidbits of info are not necessary. The other three are not necessary becuase they default to certain values when left blank. **Username** defaults to the format `LastNameFirstInitial`, **email** defaults to the format `FirstName.LastName@cowen.com`, and **Phone number** defaults to the value `TBA` (meaining "To be announced").
This information is easy to keep track of in the single user creation section, but it is a little less so in the multi-user section. The multi-user section takes in either a .csv or a .txt file. Each line of the file represents a single user, with information separated by commas. There are 6 fields of information and so there should be 5 commas present on each line. It follows the format:

```
First name,Last Name,Username,Email,Phone Number,ISA Number
```

As stated before, some info is mandatory, and some can be left out. If information needs to be left out, simply take out the info and leave the comma present. Here is an example file:

```
Jeff,Bridges,JBridge,JBridge@ramius.com,646-562-1001,541-9387
johnny,walker,,,516-978-1986,837-2874
jim,dandy,
jeremiah,smith,smithj,,,534-2198
jane,jeffries,,,,
```

Output log:
```
Line 1 User Sheet created successfully.
Name: Jeff Bridges
Username: JBridge
Email: JBridge@ramius.com
Phone number: 646-562-1001
ISA number: 541-9387

Line 2 User Sheet created successfully.
Name: Johnny Walker
Username: WalkerJ
Email: Johnny.Walker@cowen.com
Phone number: 516-978-1986
ISA number: 837-2874

Problem in Line 3.
Not enough info. Check for lack of commas.
Did not create New User Sheet for this line.

Line 4 User Sheet created successfully.
Name: Jeremiah Smith
Username: smithj
Email: Jeremiah.Smith@cowen.com
Phone number: TBA
ISA number: 534-2198

Problem in Line 5.
Left ISA number blank.
Did not create New User Sheet for this line.
```
Note that when the application encounters a problem, it doesn't crash, but instead skips the creation process for that line and takes note of the problem in the output log.
### Development

NESW is in it's first stage of development. It is technically a final product, but there are a few more things that can be added to make the experience a bit more intuitive and perhaps a tad more helpful. If you encounter any bugs, or you have any ideas for possible additions or edits that can be made to the application, please contact the developer at PellegrinoSoftwareSolutions@gmail.com.

### To Do

 - Run extensive tests
 - Add functionality to prevent auto-overrite of files with the same name
 - Revise code comments and create Javadocs

License
----

MIT


**Anyway, have fun!**