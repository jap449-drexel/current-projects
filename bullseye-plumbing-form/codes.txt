257
150.00
Pipe cleaning - Cleaned pipes using a high density
brass cleaner. The cleaner is first primed, then 
emptied into the pipes in such a way, that the pipes
can be properly cleaned and unclogged.
$$$$
300
1,000.00
Cesspool repair - Repaired the main structure of the
cesspool. Applied a strong adhesive to the cracked
frame and sprayed a coat of sealant to prevent further
damaging.
$$$$
124
75.00
Pipe rerouting - Channeled pipes to other pipes to
create space that was previously non-existent. 
Added a T-joint to a pipe in order to join it with
a second pipe, which then connected to a pipe 
parallel to it.  
$$$$